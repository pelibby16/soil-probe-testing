// SPDX-FileCopyrightText: 2022 Limor Fried for Adafruit Industries
//
// SPDX-License-Identifier: MIT

#include <Arduino.h>
#include "Adafruit_MAX1704X.h"
#include <Adafruit_NeoPixel.h>
#include "Adafruit_TestBed.h"
#include <Adafruit_BME280.h>
#include <Adafruit_ST7789.h> 
#include <Fonts/FreeSans12pt7b.h>

// Left off with the softserial problem. Need a library that works with the ESP32-S3 w/ display Feather and 
// the external board. Possible to do it with a Nano, maybe some other board not as esoteric as the ESP32-S3
// https://docs.google.com/document/d/1GuohSJar6o_BN3evtt9IEiNKa_WyzTsChB2vMDnMM7w/edit#heading=h.1mcgooz5m62
// https://embeddedthere.com/how-to-interface-esp32-with-rs485-modbus-sensors-with-example-code/
// https://www.circuitschools.com/measure-soil-npk-values-using-soil-npk-sensor-with-arduino/
// https://learn.adafruit.com/adafruit-esp32-s3-tft-feather?view=all

Adafruit_BME280 bme; // I2C
bool bmefound = false;
extern Adafruit_TestBed TB;

Adafruit_MAX17048 batteryMonitor;
Adafruit_ST7789 display = Adafruit_ST7789(TFT_CS, TFT_DC, TFT_RST);

GFXcanvas16 canvas(240, 135);

// Pins for hardware serial setup
#define RXD2 2 // 16
#define TXD2 1 // 17
#define RE 13
#define DE 12

// bytes for reaching different pieces of data
const byte phos[] = {0x01, 0x03, 0x00, 0x1f, 0x00, 0x01, 0xb5, 0xcc};
const byte nitro[] = {0x01, 0x03, 0x00, 0x1e, 0x00, 0x01, 0xe4, 0x0c};

const byte temp[] = {0x01, 0x03, 0x00, 0x12, 0x00, 0x02, 0x64, 0x0e};

byte values[11];

// add a comment here
void setup() {
  Serial.begin(115200);

  // Serial 2 uses RX TX pins on the board
  // 9600 / 8n1 should be correct
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  
  delay(100);
  
  // turn on the TFT / I2C power supply
  pinMode(TFT_I2C_POWER, OUTPUT);
  digitalWrite(TFT_I2C_POWER, HIGH);

  pinMode(NEOPIXEL_POWER, OUTPUT);
  digitalWrite(NEOPIXEL_POWER, HIGH);
  delay(10);
  
  TB.neopixelPin = PIN_NEOPIXEL;
  TB.neopixelNum = 1; 
  TB.begin();
  TB.setColor(WHITE);

  display.init(135, 240);           // Init ST7789 240x135
  display.setRotation(3);
  canvas.setFont(&FreeSans12pt7b);
  canvas.setTextColor(ST77XX_WHITE); 

  if (!batteryMonitor.begin()) {
    Serial.println(F("Could not find Adafruit MAX17048\nPlug the battery in if you have one\n"));
    delay(10); 
  } else {
    Serial.println("Found MAX17048");
    Serial.print("Version: 0x"); Serial.println(batteryMonitor.getICversion(), HEX);
  }

  if (TB.scanI2CBus(0x77)) {
    Serial.println("BME280 address");

    unsigned status = bme.begin();  
    if (!status) {
      Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
      Serial.print("SensorID was: 0x"); Serial.println(bme.sensorID(),16);
      Serial.print("        ID of 0xFF probably means a bad address, a BMP 180 or BMP 085\n");
      Serial.print("   ID of 0x56-0x58 represents a BMP 280,\n");
      Serial.print("        ID of 0x60 represents a BME 280.\n");
      Serial.print("        ID of 0x61 represents a BME 680.\n");
      return;
    }
    Serial.println("BME280 found OK");
    bmefound = true;
  }
}

uint8_t j = 0;

void loop() {
  byte val1, val2, val3;
  val1 = temperature();
  //val3 = nitrogen();
  //Serial.println(val2);

  if (j % 5 == 0) {
    canvas.fillScreen(ST77XX_BLACK);
    canvas.setCursor(0, 25);
    canvas.setTextColor(ST77XX_RED);
    canvas.println("Porter test 1");
    canvas.setTextColor(ST77XX_YELLOW);
    canvas.print("Temp: ");
    canvas.println(val1);
    // canvas.setTextColor(ST77XX_GREEN); 
    // canvas.print("Battery: ");
    // canvas.setTextColor(ST77XX_WHITE);
    // canvas.print(batteryMonitor.cellVoltage(), 1);
    // canvas.print(" V  /  ");
    // canvas.print(batteryMonitor.cellPercent(), 0);
    // canvas.println("%");
    // canvas.setTextColor(ST77XX_BLUE); 
    // canvas.print("I2C: ");
    // canvas.setTextColor(ST77XX_WHITE);
    // for (uint8_t a=0x01; a<=0x7F; a++) {
    //   if (TB.scanI2CBus(a, 0))  {
    //     canvas.print("0x");
    //     canvas.print(a, HEX);
    //     canvas.print(", ");
    //   }
    // }
    display.drawRGBBitmap(0, 0, canvas.getBuffer(), 240, 135);
    pinMode(TFT_BACKLITE, OUTPUT);
    digitalWrite(TFT_BACKLITE, HIGH);
  }
  
  TB.setColor(TB.Wheel(j++));
  delay(10);
  return;
}
byte temperature() {
  Serial2.flush();    // Not sure about this part
  digitalWrite(DE, HIGH);
  digitalWrite(RE, HIGH);
  delay(1);
  for (uint8_t i = 0; i < sizeof(temp); i++ ) Serial2.write( temp[i] );
  Serial2.flush();
  digitalWrite(DE, LOW);
  digitalWrite(RE, LOW);
// delay to allow response bytes to be received!
  delay(200);
  for (byte i = 0; i < 7; i++) {
    values[i] = Serial2.read();
    Serial.print(values[i], HEX);
    Serial.print(' ');
  }
  Serial.println("");
  return values[4];
}
byte phosphorous() {
  Serial2.flush();    // Not sure about this part
  digitalWrite(DE, HIGH);
  digitalWrite(RE, HIGH);
  delay(1);
  for (uint8_t i = 0; i < sizeof(phos); i++ ) Serial2.write( phos[i] );
  Serial2.flush();
  digitalWrite(DE, LOW);
  digitalWrite(RE, LOW);
  // delay to allow response bytes to be received!
  delay(200);
  for (byte i = 0; i < 7; i++) {
    values[i] = Serial2.read();
    Serial.print(values[i], HEX);
    Serial.print(' ');
  }
  return values[4];
}
byte nitrogen() {
  Serial2.flush();
  digitalWrite(DE, HIGH);
  digitalWrite(RE, HIGH);
  delay(1);

  // write out the message
  for (uint8_t i = 0; i < sizeof(nitro); i++ ) Serial2.write( nitro[i] );

  // wait for the transmission to complete
  Serial2.flush();
  
  // switching RS485 to receive mode
  digitalWrite(DE, LOW);
  digitalWrite(RE, LOW);

  // delay to allow response bytes to be received!
  delay(200);

  // read in the received bytes
  for (byte i = 0; i < 7; i++) {
    values[i] = Serial2.read();
    Serial.print(values[i], HEX);
    Serial.print(' ');
  }
  return values[4];
}